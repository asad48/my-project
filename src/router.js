import Vue from 'vue'
import Router from 'vue-router'

import Home from "@/views/Home.vue"
import First from "@/views/First.vue"
import Second from "@/views/Second.vue"
import Third from "@/views/Third.vue"
import Fourth from "@/views/Fourth.vue"
import Fifth from "@/views/Fifth.vue"

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior() {
    return { x: 0, y: 0 }
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/first',
      name: 'first',
      component: First
    },
    {
      path: '/second',
      name: 'second',
      component: Second
    },
    {
      path: '/third',
      name: 'third',
      component: Third
    },
    {
      path: '/fourth',
      name: 'fourth',
      component: Fourth
    },
    {
      path: '/fifth',
      name: 'first',
      component: Fifth
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})

export default router
